package com.aramco.ode3.service;

import com.aramco.ode3.model.ode.Lorenz;
import com.aramco.ode3.model.ode.ODESystemSolution;
import com.aramco.ode3.model.request.LorenzRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class LorenzService {

    public ODESystemSolution solve(LorenzRequest lorenzRequest) {
        Lorenz lorenz = new Lorenz(lorenzRequest);
        return lorenz.solve();
    }
}
