package com.aramco.ode3.service;

import com.aramco.ode3.model.genetic.GeneticOptimizationData;
import com.aramco.ode3.model.genetic.SimpleOptimizationFunctional;
import com.aramco.ode3.model.ode.ODESystemSolution;
import io.jenetics.DoubleGene;
import io.jenetics.MeanAlterer;
import io.jenetics.Mutator;
import io.jenetics.Phenotype;
import io.jenetics.engine.Engine;
import io.jenetics.engine.EvolutionResult;
import io.jenetics.engine.EvolutionStatistics;
import io.jenetics.ext.moea.Vec;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.function.Consumer;

import static io.jenetics.engine.EvolutionResult.toBestPhenotype;
import static io.jenetics.engine.Limits.bySteadyFitness;

@RequiredArgsConstructor
@Service
public class GeneticsOptimizationService {

    public double[][] optimize(ODESystemSolution solution, GeneticOptimizationData config) {
        SimpleOptimizationFunctional simpleOptimizationFunctional = new SimpleOptimizationFunctional(solution);

        Engine<DoubleGene, Vec<double[]>> engine = Engine
                // Create a new builder with the given fitness
                // function and chromosome.
                .builder(
                        simpleOptimizationFunctional::matrixFitness,
                        simpleOptimizationFunctional.codecs()
                )
                .populationSize(config.getPopulationSize())
                .minimizing()
                .alterers(
                        new Mutator<>(config.getMutatorProbability()),
                        new MeanAlterer<>(config.getMeanAltererProbability()))
                // Build an evolution engine with the
                // defined parameters.
                .build();

        Consumer<? super EvolutionResult<DoubleGene, Vec<double[]>>> statistics = EvolutionStatistics.ofComparable();
        final Phenotype<DoubleGene, Vec<double[]>> best = engine.stream()
                // Truncate the evolution stream after 7 "steady"
                // generations.
                .limit(bySteadyFitness(config.getGenerationLimit()))
                // Update the evaluation statistics after
                // each generation
                .peek(statistics)
                // Collect (reduce) the evolution stream to
                // its best phenotype.
                .collect(toBestPhenotype());

        System.out.println(statistics);
        System.out.println(best);

        return SimpleOptimizationFunctional.formatMatrix(
                best.genotype().stream().mapToDouble(value -> value.gene().doubleValue()).toArray());
    }

}
