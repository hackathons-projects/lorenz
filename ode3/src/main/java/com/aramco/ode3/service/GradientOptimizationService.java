package com.aramco.ode3.service;

import com.aramco.ode3.model.Percentage;
import com.aramco.ode3.model.gradient.GradientAlgorithm;
import com.aramco.ode3.model.gradient.GradientOptimizationData;
import com.aramco.ode3.model.gradient.GradientResultModel;
import org.springframework.stereotype.Service;

import java.util.function.BiFunction;

@Service
public class GradientOptimizationService {

    /**
     * Optimizer
     *
     * @return - optimal functional parameters and total loss
     */
    public GradientResultModel optimize(GradientOptimizationData gradientOptimizationData,
                                        BiFunction<GradientOptimizationData, Integer, Double> optimizationFunctional,
                                        GradientAlgorithm algorithm,
                                        int epochs) {
        GradientResultModel result = null;
        for (int index = 0; index < epochs; index++) {
            switch (algorithm) {
                case BGD -> result = BGD(gradientOptimizationData, optimizationFunctional);
                case SGD -> result = SGD(gradientOptimizationData, optimizationFunctional);
            }
        }

        return result;
    }

    public GradientResultModel SGD(
            GradientOptimizationData gradientOptimizationData,
            BiFunction<GradientOptimizationData, Integer, Double> optimizationFunctional
    ) {
        Percentage gradientPercentage = new Percentage(gradientOptimizationData.getResults().length, "Gradient percentage");
        for (int j = 0; j < gradientOptimizationData.getResults().length; j++) {
            double functionalPoint = optimizationFunctional.apply(gradientOptimizationData, j);

            double gradient = (functionalPoint) * gradientOptimizationData.getFeatures()[j][0];
            gradientOptimizationData.getParameters()[0] =
                    gradientOptimizationData.getParameters()[0] - 2 * gradientOptimizationData.getLearningRate() * gradient;

            gradient = (functionalPoint) * gradientOptimizationData.getFeatures()[j][1];
            gradientOptimizationData.getParameters()[1] =
                    gradientOptimizationData.getParameters()[1] - 2 * gradientOptimizationData.getLearningRate() * gradient;

            gradient = (functionalPoint) * gradientOptimizationData.getFeatures()[j][2];
            gradientOptimizationData.getParameters()[2] =
                    gradientOptimizationData.getParameters()[2] - 2 * gradientOptimizationData.getLearningRate() * gradient;

            gradient = (functionalPoint);
            gradientOptimizationData.getParameters()[3] =
                    gradientOptimizationData.getParameters()[3] - 2 * gradientOptimizationData.getLearningRate() * gradient;
            gradientPercentage.increment();
        }

        Percentage lossPercentage = new Percentage(gradientOptimizationData.getResults().length, "Loss percentage");
        double totalLoss = 0;
        for (int j = 0; j < gradientOptimizationData.getResults().length; j++) {
            double functionalPoint = optimizationFunctional.apply(gradientOptimizationData, j);
            totalLoss = totalLoss + Math.pow(functionalPoint, 2);
            lossPercentage.increment();
        }
        return new GradientResultModel(gradientOptimizationData.getParameters(), totalLoss);
    }

    public GradientResultModel BGD(
            GradientOptimizationData gradientOptimizationData,
            BiFunction<GradientOptimizationData, Integer, Double> optimizationFunctional
    ) {
        Percentage gradientPercentageA1 = new Percentage(gradientOptimizationData.getResults().length,
                "Gradient Stochastic A1 percentage");
        double sum = 0;
        for (int j = 0; j < gradientOptimizationData.getResults().length; j++) {
            sum = sum + (optimizationFunctional.apply(gradientOptimizationData, j)) * gradientOptimizationData.getFeatures()[j][0];
            gradientPercentageA1.increment();
        }
        double updateValue = 2 * gradientOptimizationData.getLearningRate() * sum / gradientOptimizationData.getResults().length;
        gradientOptimizationData.getParameters()[0] = gradientOptimizationData.getParameters()[0] - updateValue;

        Percentage gradientPercentageA2 = new Percentage(gradientOptimizationData.getResults().length,
                "Gradient Stochastic A2 percentage");
        sum = 0;
        for (int j = 0; j < gradientOptimizationData.getResults().length; j++) {
            sum = sum + (optimizationFunctional.apply(gradientOptimizationData, j)) * gradientOptimizationData.getFeatures()[j][1];
            gradientPercentageA2.increment();
        }
        updateValue = 2 * gradientOptimizationData.getLearningRate() * sum / gradientOptimizationData.getResults().length;
        gradientOptimizationData.getParameters()[1] = gradientOptimizationData.getParameters()[1] - updateValue;

        Percentage gradientPercentageA3 = new Percentage(gradientOptimizationData.getResults().length,
                "Gradient Stochastic A3 percentage");
        sum = 0;
        for (int j = 0; j < gradientOptimizationData.getResults().length; j++) {
            sum = sum + (optimizationFunctional.apply(gradientOptimizationData, j)) * gradientOptimizationData.getFeatures()[j][2];
            gradientPercentageA3.increment();
        }
        updateValue = 2 * gradientOptimizationData.getLearningRate() * sum / gradientOptimizationData.getResults().length;
        gradientOptimizationData.getParameters()[2] = gradientOptimizationData.getParameters()[2] - updateValue;

        Percentage gradientPercentageB = new Percentage(gradientOptimizationData.getResults().length,
                "Gradient Stochastic B percentage");
        sum = 0;
        for (int j = 0; j < gradientOptimizationData.getResults().length; j++) {
            sum = sum + (optimizationFunctional.apply(gradientOptimizationData, j));
            gradientPercentageB.increment();
        }
        updateValue = 2 * gradientOptimizationData.getLearningRate() * sum / gradientOptimizationData.getResults().length;
        gradientOptimizationData.getParameters()[3] = gradientOptimizationData.getParameters()[3] - updateValue;

        Percentage lossPercentage = new Percentage(gradientOptimizationData.getResults().length, "Loss percentage");
        double totalLoss = 0;
        for (int j = 0; j < gradientOptimizationData.getResults().length; j++) {
            totalLoss = totalLoss + Math.pow((optimizationFunctional.apply(gradientOptimizationData, j)), 2);
            lossPercentage.increment();
        }
        return new GradientResultModel(gradientOptimizationData.getParameters(), totalLoss);
    }

}
