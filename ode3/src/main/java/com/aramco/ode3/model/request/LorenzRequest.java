package com.aramco.ode3.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.math3.util.Pair;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class LorenzRequest {
    public double sigma;    // d
    public double rho;    // p
    public double beta;      // B

    private double[] y0;

    private double stepSize;
    private double h;
    private Pair<Double, Double> interval;
}
