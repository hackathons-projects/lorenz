package com.aramco.ode3.model.gradient;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * * @param features     - solution points (x, y, z)
 * * @param results      - function value (x', y')
 * * @param learningRate - gradient learning rate
 * * @param parameters   - optimization functional parameters
 */
@AllArgsConstructor
@Data
public class GradientOptimizationData {
    private double[][] features;
    private double[] results;
    private double learningRate;
    private double[] parameters;
}
