package com.aramco.ode3.model.gradient;

public enum GradientAlgorithm {
    BGD, SGD
}
