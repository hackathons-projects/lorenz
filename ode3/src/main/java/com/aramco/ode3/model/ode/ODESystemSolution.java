package com.aramco.ode3.model.ode;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ODESystemSolution {
    private List<Double> t;
    private List<List<Double>> y;
    private List<List<Double>> yDot;
}
