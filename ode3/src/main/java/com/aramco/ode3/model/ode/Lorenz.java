package com.aramco.ode3.model.ode;

import com.aramco.ode3.model.Percentage;
import com.aramco.ode3.model.request.LorenzRequest;
import lombok.Data;
import org.apache.commons.math3.ode.nonstiff.ClassicalRungeKuttaIntegrator;
import org.apache.commons.math3.ode.sampling.FixedStepHandler;
import org.apache.commons.math3.ode.sampling.StepNormalizer;

import java.util.ArrayList;
import java.util.Arrays;

@Data
public class Lorenz implements FixedStepHandler {

    private Percentage percentage;
    private ODESystemSolution odeSystemSolution;
    private LorenzEquations equations;
    private ClassicalRungeKuttaIntegrator integrator;
    private LorenzRequest lorenzRequest;

    public Lorenz(LorenzRequest lorenzRequest) {
        equations = new LorenzEquations(lorenzRequest.getSigma(), lorenzRequest.getRho(), lorenzRequest.getBeta());
        integrator = new ClassicalRungeKuttaIntegrator(lorenzRequest.getStepSize());
        integrator.addStepHandler(new StepNormalizer(lorenzRequest.getH(), this));
        this.lorenzRequest = lorenzRequest;
    }

    public ODESystemSolution solve() {
        integrator.integrate(
                equations,
                this.lorenzRequest.getInterval().getFirst(),
                this.lorenzRequest.getY0(),
                this.lorenzRequest.getInterval().getSecond(),
                new double[3]
        );
        return odeSystemSolution;
    }

    @Override
    public void init(double t0, double[] y0, double t) {
        percentage = new Percentage(
                (long) ((lorenzRequest.getInterval().getSecond() - lorenzRequest.getInterval().getFirst()) / lorenzRequest.getStepSize()),
                "ODE solve percent: "
        );
        odeSystemSolution = new ODESystemSolution(new ArrayList<>(),
                Arrays.asList(new ArrayList<>(), new ArrayList<>(), new ArrayList<>()),
                Arrays.asList(new ArrayList<>(), new ArrayList<>(), new ArrayList<>())
        );
    }

    @Override
    public void handleStep(double t, double[] y, double[] yDot, boolean isLast) {
        odeSystemSolution.getT().add(t);
        odeSystemSolution.getY().get(0).add(y[0]);
        odeSystemSolution.getY().get(1).add(y[1]);
        odeSystemSolution.getY().get(2).add(y[2]);
        odeSystemSolution.getYDot().get(0).add(yDot[0]);
        odeSystemSolution.getYDot().get(1).add(yDot[1]);
        odeSystemSolution.getYDot().get(2).add(yDot[2]);
        percentage.increment();
    }
}
