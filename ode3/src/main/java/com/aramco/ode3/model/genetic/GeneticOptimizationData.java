package com.aramco.ode3.model.genetic;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class GeneticOptimizationData {
    private int populationSize;
    private double mutatorProbability;
    private double meanAltererProbability;
    private int generationLimit;
}
