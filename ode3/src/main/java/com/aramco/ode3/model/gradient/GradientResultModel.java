package com.aramco.ode3.model.gradient;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class GradientResultModel {
    private double[] parameters;
    private double totalLoss;
}
