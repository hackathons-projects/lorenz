package com.aramco.ode3.model.genetic;

import com.aramco.ode3.model.ode.ODESystemSolution;
import io.jenetics.DoubleGene;
import io.jenetics.Optimize;
import io.jenetics.engine.Codecs;
import io.jenetics.engine.InvertibleCodec;
import io.jenetics.ext.moea.Vec;
import io.jenetics.ext.moea.VecFactory;
import io.jenetics.util.DoubleRange;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.IntStream;

@Slf4j
@RequiredArgsConstructor
@Getter
public class SimpleOptimizationFunctional {

    private AtomicLong counter = new AtomicLong(0);

    public static double[][] formatMatrix(double[] a) {
        return new double[][]{
                new double[]{a[0], a[1], a[2], a[3], a[4], a[5]},
                new double[]{a[6], a[7], a[8], a[9], a[10], a[11]},
                new double[]{a[12], a[13], a[14], a[15], a[16], a[17]}
        };
    }

    private final ODESystemSolution odeSystemSolution;

    public Vec<double[]> matrixFitness(final double[] input) {
        long stage = counter.incrementAndGet();
        return VecFactory.ofDoubleVec(
                Optimize.MINIMUM,
                Optimize.MINIMUM,
                Optimize.MINIMUM
        ).newVec(new double[]{
                sum(odeSystemSolution, 0, new double[]{input[0], input[1], input[2], input[3], input[4], input[5]}, stage),
                sum(odeSystemSolution, 1, new double[]{input[6], input[7], input[8], input[9], input[10], input[11]}, stage),
                sum(odeSystemSolution, 2, new double[]{input[12], input[13], input[14], input[15], input[16], input[17]}, stage)
        });
    }

    public InvertibleCodec<double[], DoubleGene> codecs() {
        return Codecs.ofVector(
                DoubleRange.of(-30, 30), DoubleRange.of(-30, 30), DoubleRange.of(-30, 30), DoubleRange.of(-30, 30), DoubleRange.of(-30, 30), DoubleRange.of(-30, 30),
                DoubleRange.of(-30, 30), DoubleRange.of(-30, 30), DoubleRange.of(-30, 30), DoubleRange.of(-30, 30), DoubleRange.of(-30, 30), DoubleRange.of(-30, 30),
                DoubleRange.of(-30, 30), DoubleRange.of(-30, 30), DoubleRange.of(-30, 30), DoubleRange.of(-30, 30), DoubleRange.of(-30, 30), DoubleRange.of(-30, 30)
        );
    }

    private double sum(ODESystemSolution odeSystemSolution, int eqIndex, final double[] a, long stage) {
        log.info("Stage: " + stage + ": Calculate yDot[" + eqIndex + "]");
        return IntStream.range(0, odeSystemSolution.getT().size()).parallel()
                .mapToDouble(index -> {
                    return equation(
                            odeSystemSolution.getYDot().get(eqIndex).stream().mapToDouble(i -> i).toArray(),
                            new double[]{
                                    odeSystemSolution.getY().get(0).get(index),
                                    odeSystemSolution.getY().get(1).get(index),
                                    odeSystemSolution.getY().get(2).get(index)
                            },
                            odeSystemSolution.getT().get(index),
                            a

                    );
                }).sum();
    }

    private double equation(final double[] yDot, final double[] y, final double t, final double[] a) {
        return Math.pow(
                -yDot[0] +
                        a[0] * y[0] + a[1] * y[1] + a[2] + y[2] +
                        a[3] * y[0] * y[1] + a[4] * y[0] * y[2] + a[5] * y[1] * y[2]
                , 2);
    }
}
