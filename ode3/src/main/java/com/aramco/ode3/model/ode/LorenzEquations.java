package com.aramco.ode3.model.ode;

import lombok.Data;
import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.exception.MaxCountExceededException;
import org.apache.commons.math3.ode.FirstOrderDifferentialEquations;

@Data
public class LorenzEquations implements FirstOrderDifferentialEquations {

    public double sigma;    // d
    public double rho;    // p
    public double beta;      // B

    public LorenzEquations(double sigma, double rho, double B) {
        this.sigma = sigma;
        this.rho = rho;
        this.beta = B;
    }

    @Override
    public int getDimension() {
        return 3;
    }

    @Override
    public void computeDerivatives(double t, double[] y, double[] yDot) throws MaxCountExceededException, DimensionMismatchException {
        yDot[0] = sigma * (y[1] - y[0]);        // dx/dt = d(y-x)
        yDot[1] = y[0] * (rho - y[2]) - y[1];    // dy/dt = x(p-z)-y
        yDot[2] = y[0] * y[1] - beta * y[2];      // dz/dt = xy - Bz
    }
}
