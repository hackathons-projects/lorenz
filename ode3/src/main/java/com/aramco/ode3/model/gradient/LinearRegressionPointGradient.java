package com.aramco.ode3.model.gradient;

import lombok.NoArgsConstructor;

import java.util.function.BiFunction;

@NoArgsConstructor
public class LinearRegressionPointGradient implements BiFunction<GradientOptimizationData, Integer, Double> {

    @Override
    public Double apply(GradientOptimizationData config, Integer index) {
        double[] parameters = config.getParameters();
        double[][] features = config.getFeatures();
        double[] results = config.getResults();

        return parameters[0] * features[index][0] + parameters[1] * features[index][1]
                + parameters[2] * features[index][2] + parameters[3] - results[index];
    }
}
