package com.aramco.ode3.controller;

import com.aramco.ode3.model.genetic.GeneticOptimizationData;
import com.aramco.ode3.model.ode.ODESystemSolution;
import com.aramco.ode3.model.request.LorenzRequest;
import com.aramco.ode3.service.GeneticsOptimizationService;
import com.aramco.ode3.service.LorenzService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.math3.util.Pair;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RequestMapping("/ode")
@RestController
public class LorenzController {

    private final LorenzService lorenzService;
    private final GeneticsOptimizationService geneticsOptimizationService;

    @GetMapping("/lorenz/butterfly")
    public ODESystemSolution strangeAttractor(@RequestParam(required = false, defaultValue = "0.001") Double stepSize,
                                              @RequestParam(required = false, defaultValue = "0.001") Double h,
                                              @RequestParam(required = false, defaultValue = "0.") Double t0,
                                              @RequestParam(required = false, defaultValue = "100.") Double tn
    ) {
        return lorenzService.solve(new LorenzRequest(
                10, 28., 8. / 3.,
                new double[]{0., 10., 20.},
                stepSize,
                h,
                Pair.create(t0, tn)
        ));
    }

    @PostMapping("/lorenz")
    public ODESystemSolution solveLorenzSystem(@RequestBody LorenzRequest lorenzRequest) {
        return lorenzService.solve(lorenzRequest);
    }

    @GetMapping("/lorenz/butterfly/restoreParams")
    public double[][] restoreParams(
            @RequestParam(required = false, defaultValue = "0.001") Double stepSize,
            @RequestParam(required = false, defaultValue = "0.001") Double h,
            @RequestParam(required = false, defaultValue = "0.") Double t0,
            @RequestParam(required = false, defaultValue = "100.") Double tn,
            @RequestParam(required = false, defaultValue = "50") Integer populationSize,
            @RequestParam(required = false, defaultValue = "0.1") Double mutatorProbability,
            @RequestParam(required = false, defaultValue = "0.2") Double meanAltererProbability,
            @RequestParam(required = false, defaultValue = "100") Integer generationLimit
    ) {
        ODESystemSolution solution = lorenzService.solve(new LorenzRequest(
                10, 28., 8. / 3.,
                new double[]{0., 10., 20.},
                stepSize,
                h,
                Pair.create(t0, tn)
        ));
        return geneticsOptimizationService.optimize(solution, new GeneticOptimizationData(
                populationSize, mutatorProbability, meanAltererProbability, generationLimit
        ));
    }
}
