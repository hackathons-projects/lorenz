package com.aramco.ode3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ode3Application {

    public static void main(String[] args) {
        SpringApplication.run(Ode3Application.class, args);
    }

}
