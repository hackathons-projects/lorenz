package com.aramco.ode3;

import io.jenetics.*;
import io.jenetics.engine.Codecs;
import io.jenetics.engine.Engine;
import io.jenetics.engine.EvolutionResult;
import io.jenetics.engine.EvolutionStatistics;
import io.jenetics.ext.moea.Vec;
import io.jenetics.ext.moea.VecFactory;
import io.jenetics.util.DoubleRange;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.function.Consumer;

import static io.jenetics.engine.EvolutionResult.toBestPhenotype;
import static io.jenetics.engine.Limits.bySteadyFitness;

@SpringBootTest
public class GeneticTest {

    // The fitness function.
    private static double fitness(final double x) {
        return Math.pow(x, 2);
    }

    @Test
    public void geneticTest() {
        final Engine<DoubleGene, Double> engine = Engine
                // Create a new builder with the given fitness
                // function and chromosome.
                .builder(
                        GeneticTest::fitness,
                        Codecs.ofScalar(DoubleRange.of(-10, 10)))
                .populationSize(500)
                .optimize(Optimize.MINIMUM)
                .alterers(
                        new Mutator<>(1E-6),
                        new MeanAlterer<>(1E-2))
                // Build an evolution engine with the
                // defined parameters.
                .build();

        // Create evolution statistics consumer.
        final EvolutionStatistics<Double, ?>
                statistics = EvolutionStatistics.ofNumber();

        final Phenotype<DoubleGene, Double> best = engine.stream()
                // Truncate the evolution stream after 7 "steady"
                // generations.
                .limit(bySteadyFitness(7))
                // The evolution will stop after maximal 100
                // generations.
                .limit((int) 1E2)
                // Update the evaluation statistics after
                // each generation
                .peek(statistics)
                // Collect (reduce) the evolution stream to
                // its best phenotype.
                .collect(toBestPhenotype());

        System.out.println(statistics);
        System.out.println(best);
    }

    static double polynomFitness(final double[] point) {
        final double x = point[0];
        final double y = point[1];
        return x / Math.abs(y);
    }

    @Test
    public void geneticPolynomMVOTest() {
        Engine<DoubleGene, Double> engine = Engine
                // Create a new builder with the given fitness
                // function and chromosome.
                .builder(
                        GeneticTest::polynomFitness,
                        Codecs.ofVector(DoubleRange.of(5, 10), DoubleRange.of(-1, 10))
                )
                .populationSize(50)
                .minimizing()
                .alterers(
                        new Mutator<>(1E-2),
                        new MeanAlterer<>(1E-1))
                // Build an evolution engine with the
                // defined parameters.
                .build();

        Consumer<? super EvolutionResult<DoubleGene, Double>> statistics = EvolutionStatistics.ofComparable();
        final Phenotype<DoubleGene, Double> best = engine.stream()
                // Truncate the evolution stream after 7 "steady"
                // generations.
                .limit(bySteadyFitness(1000))
                // Update the evaluation statistics after
                // each generation
                .peek(statistics)
                // Collect (reduce) the evolution stream to
                // its best phenotype.
                .collect(toBestPhenotype());

        System.out.println(statistics);
        System.out.println(best);
    }

    static Vec<double[]> matrixFitness(final double[] input) {
        return VecFactory.ofDoubleVec(
                Optimize.MINIMUM,
                Optimize.MAXIMUM
        ).newVec(new double[]{
                input[0] + input[1],
                input[2] + input[3]
        });
    }

    @Test
    public void geneticSystemMVOTest() {
        Engine<DoubleGene, Vec<double[]>> engine = Engine
                // Create a new builder with the given fitness
                // function and chromosome.
                .builder(
                        GeneticTest::matrixFitness,
                        Codecs.ofVector(
                                DoubleRange.of(5, 10), DoubleRange.of(-1, 10),
                                DoubleRange.of(5, 10), DoubleRange.of(-1, 10)
                        )
                )
                .populationSize(50)
                .minimizing()
                .alterers(
                        new Mutator<>(1E-2),
                        new MeanAlterer<>(1E-1))
                // Build an evolution engine with the
                // defined parameters.
                .build();

        Consumer<? super EvolutionResult<DoubleGene, Vec<double[]>>> statistics = EvolutionStatistics.ofComparable();
        final Phenotype<DoubleGene, Vec<double[]>> best = engine.stream()
                // Truncate the evolution stream after 7 "steady"
                // generations.
                .limit(bySteadyFitness(1000))
                // Update the evaluation statistics after
                // each generation
                .peek(statistics)
                // Collect (reduce) the evolution stream to
                // its best phenotype.
                .collect(toBestPhenotype());

        System.out.println(statistics);
        System.out.println(best);
    }
}
