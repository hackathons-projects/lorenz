package com.aramco.ode3;

import com.aramco.ode3.model.gradient.GradientAlgorithm;
import com.aramco.ode3.model.gradient.GradientOptimizationData;
import com.aramco.ode3.model.gradient.GradientResultModel;
import com.aramco.ode3.model.gradient.LinearRegressionPointGradient;
import com.aramco.ode3.service.GradientOptimizationService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Random;

@SpringBootTest
public class GradientTests {

    @Autowired
    private GradientOptimizationService gradientOptimizationService;

    @Test
    public void gradient() {
        // y=3*x1+4*x2+5*x3+10
        Random random = new Random();
        double[] results = new double[100];
        double[][] features = new double[100][3]; // x[], y[], z[]
        for (int i = 0; i < 100; i++) {
            for (int j = 0; j < features[i].length; j++) {
                features[i][j] = random.nextDouble();
            }
            results[i] = 3 * features[i][0] + 4 * features[i][1] + 5 * features[i][2] + 10;
        }

        double[] parameters = new double[]{1.0, 1.0, 1.0, 1.0};
        double learningRate = 0.01;
        for (int i = 0; i < 30; i++) {
            SGD(features, results, learningRate, parameters);
        }
        parameters = new double[]{1.0, 1.0, 1.0, 1.0};
        System.out.println("==========================");
        for (int i = 0; i < 3000; i++) {
            BGD(features, results, learningRate, parameters);
        }
    }

    private void SGD(double[][] features, double[] results, double learningRate, double[] parameters) {
        for (int j = 0; j < results.length; j++) {
            double gradient = (parameters[0] * features[j][0] + parameters[1] * features[j][1]
                    + parameters[2] * features[j][2] + parameters[3] - results[j]) * features[j][0];
            parameters[0] = parameters[0] - 2 * learningRate * gradient;

            gradient = (parameters[0] * features[j][0] + parameters[1] * features[j][1] + parameters[2] * features[j][2]
                    + parameters[3] - results[j]) * features[j][1];
            parameters[1] = parameters[1] - 2 * learningRate * gradient;

            gradient = (parameters[0] * features[j][0] + parameters[1] * features[j][1] + parameters[2] * features[j][2]
                    + parameters[3] - results[j]) * features[j][2];
            parameters[2] = parameters[2] - 2 * learningRate * gradient;

            gradient = (parameters[0] * features[j][0] + parameters[1] * features[j][1] + parameters[2] * features[j][2]
                    + parameters[3] - results[j]);
            parameters[3] = parameters[3] - 2 * learningRate * gradient;
        }

        double totalLoss = 0;
        for (int j = 0; j < results.length; j++) {
            totalLoss = totalLoss + Math.pow((parameters[0] * features[j][0] + parameters[1] * features[j][1]
                    + parameters[2] * features[j][2] + parameters[3] - results[j]), 2);
        }
        System.out.println(parameters[0] + " " + parameters[1] + " " + parameters[2] + " " + parameters[3]);
        System.out.println("totalLoss:" + totalLoss);
    }

    private void BGD(double[][] features, double[] results, double learningRate, double[] parameters) {
        double sum = 0;
        for (int j = 0; j < results.length; j++) {
            sum = sum + (parameters[0] * features[j][0] + parameters[1] * features[j][1]
                    + parameters[2] * features[j][2] + parameters[3] - results[j]) * features[j][0];
        }
        double updateValue = 2 * learningRate * sum / results.length;
        parameters[0] = parameters[0] - updateValue;

        sum = 0;
        for (int j = 0; j < results.length; j++) {
            sum = sum + (parameters[0] * features[j][0] + parameters[1] * features[j][1]
                    + parameters[2] * features[j][2] + parameters[3] - results[j]) * features[j][1];
        }
        updateValue = 2 * learningRate * sum / results.length;
        parameters[1] = parameters[1] - updateValue;

        sum = 0;
        for (int j = 0; j < results.length; j++) {
            sum = sum + (parameters[0] * features[j][0] + parameters[1] * features[j][1]
                    + parameters[2] * features[j][2] + parameters[3] - results[j]) * features[j][2];
        }
        updateValue = 2 * learningRate * sum / results.length;
        parameters[2] = parameters[2] - updateValue;

        sum = 0;
        for (int j = 0; j < results.length; j++) {
            sum = sum + (parameters[0] * features[j][0] + parameters[1] * features[j][1]
                    + parameters[2] * features[j][2] + parameters[3] - results[j]);
        }
        updateValue = 2 * learningRate * sum / results.length;
        parameters[3] = parameters[3] - updateValue;

        double totalLoss = 0;
        for (int j = 0; j < results.length; j++) {
            totalLoss = totalLoss + Math.pow((parameters[0] * features[j][0] + parameters[1] * features[j][1]
                    + parameters[2] * features[j][2] + parameters[3] - results[j]), 2);
        }
        System.out.println(parameters[0] + " " + parameters[1] + " " + parameters[2] + " " + parameters[3]);
        System.out.println("totalLoss:" + totalLoss);
    }

    @Test
    public void sdgTest() {
        // y=3*x1+4*x2+5*x3+10
        Random random = new Random();
        double[] results = new double[100];
        double[][] features = new double[100][3];
        for (int i = 0; i < 100; i++) {
            for (int j = 0; j < features[i].length; j++) {
                features[i][j] = random.nextDouble();
            }
            results[i] = 3 * features[i][0] + 4 * features[i][1] + 5 * features[i][2] + 10;
        }

        double[] parameters = new double[]{1.0, 1.0, 1.0, 1.0};
        double learningRate = 0.01;
        for (int i = 0; i < 30; i++) {
            GradientResultModel sgd = gradientOptimizationService.SGD(
                    new GradientOptimizationData(features, results, learningRate, parameters),
                    new LinearRegressionPointGradient()
            );
            System.out.println(sgd.getParameters()[0] + " " + sgd.getParameters()[1] + " " +
                    sgd.getParameters()[2] + " " + sgd.getParameters()[3]);
        }
    }

    @Test
    public void bdgTest() {
        // y=3*x1+4*x2+5*x3+10
        Random random = new Random();
        double[] results = new double[100];
        double[][] features = new double[100][3];
        for (int i = 0; i < 100; i++) {
            for (int j = 0; j < features[i].length; j++) {
                features[i][j] = random.nextDouble();
            }
            results[i] = 3 * features[i][0] + 4 * features[i][1] + 5 * features[i][2] + 10;
        }

        double[] parameters = new double[]{1.0, 1.0, 1.0, 1.0};
        double learningRate = 0.01;
        System.out.println("==========================");
        for (int i = 0; i < 3000; i++) {
            GradientResultModel bgd = gradientOptimizationService.BGD(
                    new GradientOptimizationData(features, results, learningRate, parameters),
                    new LinearRegressionPointGradient()
            );
            System.out.println(bgd.getParameters()[0] + " " + bgd.getParameters()[1] + " " +
                    bgd.getParameters()[2] + " " + bgd.getParameters()[3]);
            System.out.println("Total loss = " + bgd.getTotalLoss());
        }
    }

    @Test
    public void optimization() {
        // y=3*x1+4*x2+5*x3+10
        Random random = new Random();
        double[] results = new double[100];
        double[][] features = new double[100][3];
        for (int i = 0; i < 100; i++) {
            for (int j = 0; j < features[i].length; j++) {
                features[i][j] = random.nextDouble();
            }
            results[i] = 3 * features[i][0] + 4 * features[i][1] + 5 * features[i][2] + 10;
        }

        double[] parameters = new double[]{1.0, 1.0, 1.0, 1.0};
        double learningRate = 0.01;

        GradientResultModel result = gradientOptimizationService.optimize(
                new GradientOptimizationData(features, results, learningRate, parameters),
                new LinearRegressionPointGradient(),
                GradientAlgorithm.SGD, 300);

        System.out.println(result.getParameters()[0] + " " + result.getParameters()[1] + " " +
                result.getParameters()[2] + " " + result.getParameters()[3]);
        System.out.println("Total loss = " + result.getTotalLoss());

    }
}
