# Plan for research solution for ODE reconstuction

1. Generate numerical solution of Lorenz system.

 Try to use different ODE solvers Runge-Kutta45, Adams-Multon and others

2. For parameters with strange Attractor we should use several solutions with fixed step. Beacuse step size and arithmetic length can 
greatly affect the result. Also we can variate arithmetic length (We can use Java with BigDecimal as instument or Math soft(Maple/Matlab))

3. Generate many dataset of x(t), y(t), z(t) and pahse space trajectory

4. Try to generalize solution. For example we can suggest:

        x' = p_ij * x_j + X_i, where (1)

        p_ij - matrix for linear part of equations and
        X_i  - non-linear part 

        X_i = v_i * MUL(x_j, i!=j), where v - is sinature [-1, 0, 1] (2)

We call (1) systems as Lorenz-type sysytems

5. Generate some different solutions for another Lorenz-type system to enrich dataset

6. Try different neural network architectures to train several models

7. Try to reconstuct Lorenz system with generalized Lorenz-type equations, AAN and original lorenz datasets

8. Research common stucture of ODE system for any cases of dynamical systems
